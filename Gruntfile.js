module.exports = function(grunt) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> Compiled on: <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },

            /**
             * FOR Development and UAT
             **/
            development: {
                expand: true,
                options: {
                    banner: "/* <%= pkg.name %> Compiled on: <%= grunt.template.today('yyyy-mm-dd HH:MM') %> */",
                    sourceMap: false,
                    sourceMapName: 'dist/js/<%= pkg.name %>.min.js.map',
                    compress: false,
                    mangle: false,
                    preserveComments: true,
                    beautify: true // use for online debugging only
                },
                files: {
                    'build/js/<%= pkg.name %>.min.js': [
                        'temp/js/concatenated-files.js'
                    ],
                    'build/js/libs/vendors.min.js': [
                        'src/js/libs/**/*.js'
                        //'js/libs/bower_components/lodash/lodash.min.js'
                    ]
                }
            },

            /**
             * FOR Production
             **/
            production: {
                expand: false,
                options: {
                    mangle: true,
                    //mangleProperties: true, // the check first may screw things up
                    beautify: false,
                    compress: {
                        global_defs: {
                            'DEBUG': false
                        },
                        dead_code: true,

                        drop_console: true,
                        sequences: false,
                        evaluate: false,
                        loops: false,
                        unused: false
                    }
                },
                files:{
                    'build/js/<%= pkg.name %>.min.js': [
                        'temp/js/concatenated-files.js'
                        //'src/js/components/**/*.js'
                    ],
                    'build/js/libs/vendors.min.js': [
                        'src/js/libs/**/*.js'
                        //'js/libs/bower_components/lodash/lodash.min.js'
                    ]
                }
            }
        },

        copy: {
            dist: {
                expand: true,
                cwd: 'src/',
                src: [
                    'index.html',
                    'images/**/*'
                    // 'js/components/**/*.html', // TODO: possibly for templates
                    //'fonts/**/*'
                ],
                dest: 'build'
            }
        },

        concat: {
            options: {
                stripBanners: true
            },
            js: {
                src: [
                    'src/js/services/services.js',
                    'src/js/components/cart/product-catalogue.js',
                    'src/js/components/cart/user-basket.js',
                    'src/js/components/cart/cart-view.js',
                    'src/js/components/cart/cart.js'
                ],
                dest: 'temp/js/concatenated-files.js',
            },
        },

        sass: {
            dist: {
                files: {
                    'build/css/style.css': 'src/resources/sass/style.scss'
                }
            }
        },

        clean: {
            folders: [
                'build'
            ]
        },

        watch: {
            scripts: {
                files: [
                    // custom components
                    'src/js/components/**/*',

                    // common
                    'src/index.html',
                    'src/js/**/*.js',
                    'src/resources/sass/**/*.scss',

                    // configs
                    './Gruntfile.js'

                ],
                tasks: [
                    //'jshint'
                    'sass',
                    'uglify:development'
                ],
                options: {
                    interrupt: true,
                    debounceDelay: 10
                },
            },
        },

        /**
         * TODO
         * Note: need to first install: grunt-responsive-images
         * first install
         * [Graphicsmagick](http://www.graphicsmagick.org/download.html#ftp-site-organization).  graphicsmagick is in it. OR
         *
         * [Imagemagick](http://www.imagemagick.org/script/binary-releases.php#windows).  Imagemagick is in it.*
         *
         */
        responsive_images: {
            main: {
                options: {
                    engine: 'gm',

                    // Todo: still testing
                    sizes: [{
                        upscale:    true,
                        name:       '_@2x',
                        width:      '200%'
                    }]
                },
                files: [{
                    expand: true,
                    src: ['images/**/*.{gif,png,jpg}'],
                    cwd: 'src/',
                    dest: 'build/'
                }]
            }
        }

        /*
        // don't keep passwords in source control
        secret: grunt.file.readJSON('private/sftp.json'),
        sftp: {
            test: {
                files: {
                    "./": ["build/**"]
                },
                options: {
                    path: '/tmp/',
                    host: '<%= secret.host %>',
                    username: '<%= secret.username %>',
                    password: '<%= secret.password %>',
                    showProgress: true,
                    srcBasePath: "build/"
                }
            }
        },

        // Experimental only, if you are brave enougth :)
        sshexec: {
            test: {
                command: 'uptime',
                options: {
                    host: '<%= secret.host %>',
                    username: '<%= secret.username %>',
                    password: '<%= secret.password %>'
                }
            }
        }
        */
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-responsive-images');

    // TODO:
    // grunt.loadNpmTasks('grunt-contrib-concat')
    // grunt.loadNpmTasks('grunt-contrib-jshint')
    // grunt.loadNpmTasks('grunt-contrib-cssmin')
    // grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-clean');
    // grunt.loadNpmTasks('grunt-ssh'); // NOTES: I have not done it this way before, but I am experimenting locally so its fine here.

    // Default task(s).
    grunt.registerTask('default', [
        'clean',
        'concat:js',
        'uglify:development',
        'sass',
        'copy:dist',
        'watch'
    ]);


    // Production task(s).
    // todo: copy to copy images
    grunt.registerTask('production', [
        'clean',

        // TODO: fix GraphicsMagick / Imagemagick installation
        // 'responsive_images',

        'concat:js',
        'uglify:production',
        'sass',
        'copy:dist',
        'watch'
    ]);
}
