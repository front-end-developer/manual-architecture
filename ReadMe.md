## Mark Webley - Manual JavaScript Application-Architecture

## Synopsis
Test application done in a few days, includes manual labour architecture, not frameworks such as Angular, Backone, React, just libraries like Jquery to cut time. More complex responsive work can be seen in my online portfolio here: 

* My portfolio: http://tfl.adobeconsultant.co.uk/


## Dependencies

[Node](http://nodejs.org/download/).  npm is in it.

[Grunt](http://gruntjs.com/).  Gulp is in it.



## Installation

To get up and running from the app directory run:

1. npm install,
2. npm install -g grunt-cli
3. use your IDE to run your local server or node to run the files in the /build directory

## Usage

The src directory is where all the hand coded files are, the build directory is where all the processed, obfuscated,
and compiled files are. View app in your browser from the Build directory:

--root

----/build

----/src


1. run the site from build/index.html
2. if using webstorm you need to run:
localhost:63342/build/
3. can run: grunt  (for developer version in the build)
3. can run: grunt production (for obfuscated and optimized build)

## Important ( Accessibility, SEO, Indexing & Javascript )
In your test you wanted this:

"*Also ensure that the page can be viewed and used without the need for JavaScript to be enabled*"

I took liberties in doing something forward, my previous research papers at tfl.gov.uk showed this this is becoming an old idea if it is not that already. 

Because search engines such as google were loosing 30% of the market share because of not indexing Javascript Apps, this has already been caterred for (AngularJS),
aria for accessibility can be configured dynamically with Javascript (eg: modals), and Javascript is fundamental to single page apps for mobile, and tablet devices:

* pushstate: https://developer.mozilla.org/en-US/docs/Web/API/History_API
* indexed apps: http://searchengineland.com/tested-googlebot-crawls-javascript-heres-learned-220157
* SEO Google: https://developers.google.com/webmasters/ajax-crawling/docs/getting-started
* SEO Bing: http://searchengineland.com/bing-offers-recommendations-for-seo-friendly-ajax-suggests-html5-pushstate-152946

Otherwise I would use more PHP with Mysql or .Net MVC. But now .Net had changed, too web.api...web services based on the webservices.

## Application Architectures
There are other architectures such as MVC, MV* like Angular, MVVM, MVP but this one here, now-a-days I build pattern libraries because they are reusable and scalable architectures, that mean I can reuse what I code. Because time is precious, especially when you are self-employed. 


## Normally:
*I use bower to set up the library dependencies in lib.
I would have set up test cases with Jasmine - it time permitted.
I never had time to check all browsers, I checked caniuse.com as usual, I spent a bit of time setting up grunt and sass before I got to the SASS, but I just got on with it, in the time I had

I was not able to do anything last weekend...because I was moving. More time, I would have included a Hamburger menu for mobile devices, and handled the images better for IPAD and etc.*


Some of the things I wanted to do was:


| Done / todo | Description: wish list                                                  |
| ------------- | ------------------------------------------------------------ |
| [x]      | Application Architecture   |
| [x]   | Task Runners    |
| [x]      | SASS, breakpoints, mixins etc |
| [x]      | Src (hand coded source code) and deployment (build)   |
| [x]      | Continuous Integration   |
| [x]      | Responsive Web   |
| [x]      | Adaptive Web   |
| [x]      | Prepared css for retina display (example only: no images converted)   |
| [ ]      | Bin Icon, use fonts instead of images
| [x]      | Grunt automation - Optimise images for mobile web devices |
| [ ]      | Grunt automation - favicon sizes |
| [x]      | I used background image, instead of gradiant, because of none-IE 9 support http://caniuse.com/#search=gradiant   |
| [x]      | Modular Coding in JS   |
| [x]      | use localStorage for storing basket....   |
| [x]      | Implement tabbing for accessibility - parcially done, need to redo the delete structure if I had time  |
| [x]      | Note really neccesary for this task : Implement Aria for screen readers - accessibility   |
[]      | Validation |
| [  ]      | Have Calculations work from JSON data, or objects, not from the DOM   |
| [  ]      | Implement pushState for SEO   |
| [  ]      | JS: Unit Testing and Behaviour Driven Development   |
| [x]      | JS: tally up the results on click and onchange   |
| [x]      | JS: the buttons: click & change events   |
| [x]     | JS: delete item from the list   |
| [  ]      | JS: add items to the list   |
| [  ]      | Set up a central service object webservice for JSON   |
| [  ]      | connect the backend to the existing database   |
| [  ]      | backend API: Put, Delete, Get, Post   |
| [x]      | UI: set up the delete icons outside of the banding   |
| [x]      | Google fonts   |
| [  ]      | Google Analytics Measurement Protocal integration   |
| [  ]      | Nice extra: do a products page  |
| [  ]      | Angular 2.0 version   |
| [  ]      | Angular 1.x version   |
| [  ]      | React.js version   |
| [  ]      | Backbone.js version  |


## Technologies:
If I had more time would have done this in other versions also in:
Angular 1.4x, Angular 2.0, React.js, Backbone.js

## Transitions
What other front end technologies am I transitioning to now:
- From Angular 1.x too Angular 2.0, Typescript and its tool set.
- React.js and its toolset,
- from OOP PHP too .Net Web.API
