/**
 * Created by Mark Webley on 27/09/2016.
 *
 * TODO:
 * use dom.data() to store data objects or just service Objects
 */
(function($){
    console.log('cart.js');
    var subtotal = 0,
        total = 0,
        currprice = null,
        price = 0,
        cost = 0,
        vat = 0,
        vatTax = 20,
        quantity;

    /*
    * if items not in basket then get from webserver based on user session
     */
    if (userBasket.isEnabled()) {
        userBasket.getBasket();
    } else {
        // get items from DB based on user session
        // service.getUserBasketBySession();
    }


    $('.cart .btn-spinner .increment').on('click', function(evt) {
        var inp = $(this).parent().parent().find('input');
        var val = (Number(inp.val()) < 10) ? inp.val(Number(inp.val()) + 1) : 0;
        calculateSubtotal();
    });

    $('.cart .btn-spinner .decrement').on('click', function(evt) {
        var inp = $(this).parent().parent().find('input');
        var val = (Number(inp.val()) > 0) ? inp.val(Number(inp.val()) - 1) : 0;
        calculateSubtotal();
    });

    $('.cart form input.qty').on('keyup', function(evt) {
        debugger;
        var int = this.value;
        if (intOnly(int) && nNegativeInt(int)) {
            calculateSubtotal();
        } else {
            this.value = 0;
        }
    });

    $('.cart form .delete').on('click', function() {
        var inp = Number($(this).parent().find('input').val());
        var uuid = $(this).parent().find('input:eq(1)').attr('data-uuid');
        $(this).parent().remove();
        calculateSubtotal();
        userBasket.deleteProduct(uuid);
    });

    function nNegativeInt(int) {
        return (Number(int) > -1);
    }

    function intOnly(int) {
        return !isNaN(int);
    }

    /*********
     *  TODO:
     *  PUT ALL CALCULATIION IN ANOTHER JS MODULE
     */


    /**
     * TODO:
     * add product to the value Object
     */
    function addProducttoVO(obj) {
        userBasket.addToBasket(obj);
    }

    /**
     * Note to NetCentric
     * NOTE: should work from data objects not DOM but this is because of time, I
     * am just showing you how I can handle calculations, imagine the data would be replaced
     * by a data object or localStorage / sessionStorage.
     *
     * If have time make it work from JSON or localstorage data & NOT DOM...
     */
    function calculateSubtotal() {
        var uuid,
            product,
            size,
            description,
            objItem;

        subtotal = 0;

        $.each($(".cart form li.item"), function(i, item) {
            uuid = $(item).find('input:eq(1)').attr('data-uuid');
            currprice = $(item).find('.price').text();
            price =         ( Modernizr.localstorage  ) ? productCatalogue.getProduct(uuid).price : currprice.replace(/[£]+/g,"");
            quantity =      $(item).find('input.qty').val();
            cost =          ( parseFloat(price) * Number(quantity) );
            product =       ( Modernizr.localstorage  ) ? productCatalogue.getProduct(uuid).product : $(item).find('.product').text();
            description =   ( Modernizr.localstorage  ) ? productCatalogue.getProduct(uuid).description : "todo to do a data-description attribute in html5";
            size =          ( Modernizr.localstorage  ) ? productCatalogue.getProduct(uuid).size : $(item).find('input:eq(1)').attr('data-size');
            $(item).find('.cost').text( "£" + cost.toFixed(2) );
            subtotal += cost;

            objItem = {
                "uuid":         uuid,
                "quantity":     Number(quantity),
                "product":      product,
                "size":         size,
                "description":  description,
                "price":        price
            };

            if (Number(quantity) > 0) {
                addProducttoVO(objItem);
            }
        });

        vat = calculateVAT(subtotal.toFixed(2));
        total = (parseFloat(vat) + subtotal).toFixed(2);
        $(".cart .subtotal").text('£' + subtotal.toFixed(2));
        $(".cart .vat").text('£' + vat );
        $(".cart .total").text('£' + total);
    }

    function calculateVAT(subtotal) {
        return parseFloat(subtotal * vatTax / 100).toFixed(2);
    }

    /**
     * TODO:
     * serialise is just an example but I would use the productList object instead
     */
    $('.cart form').on('submit', function(evt) {
        evt.preventDefault();
        var itemsInCart = [],
            payload,
            storedItems;
        if (cartValidation()) {
            storedItems = JSON.parse(localStorage.products);
            $.each(storedItems.products, function(i, item) {
                if (item.quantity > 0) {
                    itemsInCart.push(item);
                }
            });

            payload = {
                "products" : itemsInCart,
                "invoice" : {
                    "subtotal":   subtotal.toFixed(2),
                    "vat":        vat,
                    "total" :     total
                }
            };

            console.log( JSON.stringify( payload, null, 7 )  );

            $('.page-section h1').text('Payload sent via AJAX');
            $('.page-section h5').text('The results can be seen in Console.logs. The following payload would be sent to the server, this is not complete, this is just and example');
            $('.page-body').html('<br />' + JSON.stringify(payload, null, 7));

            // TODO:
            // now send via
            $.ajax ({
                'data':     payload,
                'dataType': 'json',
                'url':      'test.php',
                'method':   'POST'

            })
                .done(function(data) {
                    console.log('SUCCESS - to some content from DOM or go to a new page');
            })
                .fail(function( jqXHR, textStatus, errorThrown) {
                    console.warn('WARNING - error in sending data');
            });
        } else {
            console.log('error form show some error info in the UI');
        }
    });

    /**
     * TODO:
     * place into a seperate module
     * return true or false based on validation
     */
    function cartValidation() {
        console.log('cartValidation');
        return true; // just testing
    }

    calculateSubtotal();

})($);



