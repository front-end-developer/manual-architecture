/**
 * Created by Mark W on 29.9.2016.
 */

var userBasket = (function() {
    var basket = localStorage;

    /**
     * TODO: get data from webservices
     * Dummy to show add items from basket to storage
     */
    function itemsInMyBasket() {
        var items = {
            "products" : [
                {
                    "uuid":         "2e6c4202-8658-11e6-ae22-56b6b6499611",
                    "quantity":     1,
                    "product":      "Cotton T-Shirt",
                    "size":         "Medium",
                    "description":  "Lovely cotton t-shirt.",
                    "price":        "1.99"
                },
                {
                    "uuid":         "2e6c446e-8658-11e6-ae22-56b6b6499611",
                    "quantity":     1,
                    "product":      "Baseball Cap",
                    "size":         "One Size",
                    "description":  "Lovely baseball cap",
                    "price":        "2.99"
                },
                {
                    "uuid":         "2e6c4568-8658-11e6-ae22-56b6b6499611",
                    "quantity":     1,
                    "product":      "Swim Shorts",
                    "size":         "Medium",
                    "description":  "Lovely swim shorts",
                    "price":        "3.99"
                },
                {
                    "uuid":         "2e6c4644-8658-11e6-ae22-56b6b6499611",
                    "quantity":     1,
                    "product":      "Shirts",
                    "size":         "Medium",
                    "description":  "Lovely shirt",
                    "price":        "3.99"
                },
                {
                    "uuid":         "2e6c4716-8658-11e6-ae22-56b6b6499611",
                    "quantity":     1,
                    "product":      "Trousers",
                    "size":         "Medium",
                    "description":  "Lovely trousers",
                    "price":        "3.99"
                },
                {
                    "uuid":         "2e6c4a9a-8658-11e6-ae22-56b6b6499611",
                    "quantity":      1,
                    "product":      "Denim Jeans",
                    "size":         "Medium",
                    "description":  "Lovely Denim jeans",
                    "price":        "3.99"
                },
                {
                    "uuid":         "2e6c4b80-8658-11e6-ae22-56b6b6499611",
                    "quantity":     1,
                    "product":      "Levi Jeans",
                    "size":         "Small",
                    "description":  "Lovely Levi jeans",
                    "price":        "3.99"
                }
            ]
        };
        basket.products = JSON.stringify(items);
        console.log('basket', basket);
    }

    function deleteProduct(uuid) {
        var storedItems = JSON.parse(localStorage.products);
        for (var i=0; i < storedItems.products.length; i++) {
            if (storedItems.products[i].uuid == uuid) {
                storedItems.products.splice(i,1);
            }
        }
        localStorage.products = JSON.stringify(storedItems);
    }

    function getBasket() {
        console.log('items in basket', JSON.parse(basket.products) );
        return JSON.parse(basket.products);
    }

    /**
     * @param   should accept a an Object
     */
    function addToBasket(obj) {
        var storedItems = JSON.parse(localStorage.products);
        var productExist = storedItems.products.filter(function(item) {
           return item.uuid == obj.uuid;
        });

        if (productExist !== null) {
            $.each(storedItems.products, function(i, item) {
                if (obj.uuid == item.uuid) {
                    item.quantity = obj.quantity;
                }
            });

           localStorage.products = JSON.stringify(storedItems);
        } else {
            // add the object to the localStorage, possibly not needed
        }
    }

    function isEnabled() {
        var isAble = false;
        if ( Modernizr.localstorage  ) {
            basket = localStorage;
            itemsInMyBasket();
            isAble = true
        }
        return isAble;
    }

    return {
        isEnabled:      isEnabled,
        getBasket:      getBasket,
        addToBasket:    addToBasket,
        deleteProduct:  deleteProduct
    }
})();