/**
 * Created by Mark W on 29.9.2016.
 */

var productCatalogue = (function() {

    /**
     * @description     Example only, in production should get from a database
     * @returns         {{products: Array}}
     */
    function getProductCatalogue() {
        var items = {
            "products" : [
                {
                    "uuid":         "2e6c4202-8658-11e6-ae22-56b6b6499611",
                    "quantity":     1,
                    "product":      "Cotton T-Shirt",
                    "size":         "Medium",
                    "description":  "Lovely cotton t-shirt.",
                    "price":        "1.99"
                },
                {
                    "uuid":         "2e6c446e-8658-11e6-ae22-56b6b6499611",
                    "quantity":     1,
                    "product":      "Baseball Cap",
                    "size":         "One Size",
                    "description":  "Lovely baseball cap",
                    "price":        "2.99"
                },
                {
                    "uuid":         "2e6c4568-8658-11e6-ae22-56b6b6499611",
                    "quantity":     1,
                    "product":      "Swim Shorts",
                    "size":         "Medium",
                    "description":  "Lovely swim shorts",
                    "price":        "3.99"
                },
                {
                    "uuid":         "2e6c4644-8658-11e6-ae22-56b6b6499611",
                    "quantity":     1,
                    "product":      "Shirts",
                    "size":         "Medium",
                    "description":  "Lovely shirt",
                    "price":        "3.99"
                },
                {
                    "uuid":         "2e6c4716-8658-11e6-ae22-56b6b6499611",
                    "quantity":     1,
                    "product":      "Trousers",
                    "size":         "Medium",
                    "description":  "Lovely trousers",
                    "price":        "3.99"
                },
                {
                    "uuid":         "2e6c4a9a-8658-11e6-ae22-56b6b6499611",
                    "quantity":      1,
                    "product":      "Denim Jeans",
                    "size":         "Medium",
                    "description":  "Lovely Denim jeans",
                    "price":        "3.99"
                },
                {
                    "uuid":         "2e6c4b80-8658-11e6-ae22-56b6b6499611",
                    "quantity":     1,
                    "product":      "Levi Jeans",
                    "size":         "Small",
                    "description":  "Lovely Levi jeans",
                    "price":        "3.99"
                }
            ]
        };
        return items;
    }


    /**
     * @param   should accept an UUID
     */
    function getProduct(uuid) {
        var storedItems = getProductCatalogue().products;
        var product = storedItems.filter(function(item) {
            return item.uuid == uuid;
        });

        return product[0];
    }

    return {
        getProduct:   getProduct
    }
})();